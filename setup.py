#!/usr/bin/env python

from setuptools import setup

name = 'rfc3987'

import sys
import importlib.util
spec = importlib.util.spec_from_file_location(name, '{}/__init__.py'.format(name))
module = importlib.util.module_from_spec(spec)
sys.modules[name] = module
spec.loader.exec_module(module)

desc, _sep, long_desc = module.__doc__.partition('.')

setup(name=name,
      description=desc.strip(),
      long_description=long_desc.lstrip().format(**module.__dict__)
      )
