import sys

from . import patterns

if not sys.argv[1:]:
    print('Valid arguments are "--all" or rule names from:')
    print('  '.join(sorted(patterns)))
elif sys.argv[1] == '--all':
    for name in patterns:
        print(name + ':')
        print(patterns[name])
else:
    for name in sys.argv[1:]:
        print(patterns[name])
